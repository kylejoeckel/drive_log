import React from 'react';
import MapView from 'react-native-maps';
import { StyleSheet, View, Text } from 'react-native';
import { Input, Card, Button, Divider } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
  
class WelcomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Welcome',
      };
    
    state = {
        longitude: undefined,
        latitude: undefined
    }
    
    render() {
        return (
            <View>
                <Card>
                    <Text h1>Welcome: <Text>Username</Text></Text>
                    <Divider style={{ backgroundColor: 'blue' }} />
                    <Text>Last Sign On:</Text> 
                    <Text>Current Status:</Text> 
                </Card>
                <Card
                    >
                    <MapView
                        style = {styles.map}
                        initialRegion={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                        }}
                    />
                </Card>    
                <Card>
                <Button
                    onPress = {() => this.props.navigation.navigate('LogIn')}
                    title="Log Out"
                    color="#841584"
                    buttonStyle = {styles.button}
                    accessibilityLabel="Learn more about this purple button"
                />
                </Card>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    map: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        height: 400,
    },
    container: {
        flex: 1,
        backgroundColor: '#000000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: '#fff'
    },
    button: {
      margin: 10
    }
  });

export default WelcomeScreen;