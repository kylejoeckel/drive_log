import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, Text, View } from 'react-native';
import { Input, Card, Button } from 'react-native-elements'
import { createStackNavigator } from 'react-navigation';

class LandingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            username: undefined,
            password: undefined };
    }
    render() {
        return (
        <View>
        <Card style={styles.container}>
            <Card style={styles.container}>
            <Input
                placeholder='Username'
                leftIcon={
                    <Icon
                    name='user'
                    size={18}
                    color='black'
                    />
                }
                value = {this.state.username}
                // onChangeText={(text) => this.setState({username})}
            />
            <Input
                placeholder='Password'
                leftIcon={
                    <Icon
                    name='key'
                    size={18}
                    color='black'
                    />
                }
                value = {this.state.password}
                // onChangeText={(text) => this.setState({password})}
            />
            </Card>
            <Button
            raised
            large
            onPress = {() => this.props.navigation.navigate('Welcome')}
            buttonStyle= {styles.button} 
            icon={{name: 'sign-in', type: 'font-awesome'}}
            title='LOG IN' />
        </Card>
        <Card style={styles.container}>
        <Button
            onPress = {() => this.props.navigation.navigate('LogIn')}
            buttonStyle = {styles.button}
            title="More Info"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
        />
        <Button
            onPress = {() => this.props.navigation.navigate('LogIn')}
            title="Admin"
            color="#841584"
            buttonStyle = {styles.button}
            accessibilityLabel="Learn more about this purple button"
        />
        </Card>
        </View>
        );
    }
};
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: '#fff'
    },
    button: {
      margin: 10
    }
});

export default LandingScreen;