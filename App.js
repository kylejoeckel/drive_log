import React from 'react';
import LandingScreen from './Components/LandingScreen';
import WelcomeScreen from './Components/WelcomeScreen';
import { View } from 'react-native';
import { StackNavigator, createStackNavigator } from 'react-navigation';

const RootStack = createStackNavigator(
  {
    Landing: LandingScreen, 
    Welcome: WelcomeScreen
  },
  {
    initialRouteName: 'Landing',
    /* The header config from HomeScreen is now here */
    navigationOptions: {
      headerTitle: "My Drive Time",
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);


export default class App extends React.Component {
  render() {
    return (
    <RootStack/>
  );
  }
}